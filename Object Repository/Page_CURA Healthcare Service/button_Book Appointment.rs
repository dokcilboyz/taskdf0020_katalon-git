<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Book Appointment</name>
   <tag></tag>
   <elementGuidId>cee1bc28-8ae1-4a46-9b13-b49f2de8d5cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#btn-book-appointment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='btn-book-appointment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>db6f88e1-6866-476e-a2f9-a9350c28145f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btn-book-appointment</value>
      <webElementGuid>979c4c00-ae58-4afe-893e-deea2b91f940</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>6f31a7aa-402e-4408-938c-e1005352eebd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default</value>
      <webElementGuid>da1d83a0-be56-4669-bfbc-c905fc7cc551</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Book Appointment</value>
      <webElementGuid>7f57ae30-0f92-4e9a-a7fc-fe7f2fb774a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btn-book-appointment&quot;)</value>
      <webElementGuid>68709cb5-b425-4bf6-8351-f253e345b94c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='btn-book-appointment']</value>
      <webElementGuid>89cc402a-4de2-49f8-906a-2186c475f544</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[6]/div/button</value>
      <webElementGuid>2c39968c-35d4-4c38-b91e-f5d7d820ca74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comment'])[1]/following::button[1]</value>
      <webElementGuid>5f75ff29-71b3-45f5-9983-375b6ef076c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Date (Required)'])[1]/following::button[1]</value>
      <webElementGuid>f68f8c73-9e4a-466b-8906-53b29108e5e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare Service'])[3]/preceding::button[1]</value>
      <webElementGuid>d25e15d9-43bb-4dca-80bd-14776c56ed36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(678) 813-1KMS'])[1]/preceding::button[1]</value>
      <webElementGuid>9ae3a77b-bf13-47f4-ba9b-70e3ba376191</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Book Appointment']/parent::*</value>
      <webElementGuid>1e67ac50-ce89-4d65-ad05-a011de58f5c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>39316fa2-c49f-469e-bd04-1559c12b004b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'btn-book-appointment' and @type = 'submit' and (text() = 'Book Appointment' or . = 'Book Appointment')]</value>
      <webElementGuid>f1da525e-0a60-4978-ad53-46a4073da88e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
