<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Apply for hospital readmission</name>
   <tag></tag>
   <elementGuidId>5a519c68-eec8-42ba-a211-029aec69edf9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.checkbox-inline</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']/div/div/form/div[2]/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>446cddb5-a7cf-4696-83b8-a3ceb0279474</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>chk_hospotal_readmission</value>
      <webElementGuid>a10c575d-18b1-4c61-9707-369235fce8fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkbox-inline</value>
      <webElementGuid>e7a1fb18-4346-4259-9ac1-994f503b887d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                             Apply for hospital readmission
                        </value>
      <webElementGuid>4391e90a-3942-42fe-a6bc-b5b644ca9bca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;col-sm-offset-5 col-sm-4&quot;]/label[@class=&quot;checkbox-inline&quot;]</value>
      <webElementGuid>f60368cd-1291-480b-8144-3a4c9e69463c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[2]/div/label</value>
      <webElementGuid>90072dcc-13ff-48cd-ad02-b0c61896958d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Facility'])[1]/following::label[1]</value>
      <webElementGuid>f112e3bf-9dd7-4b9d-9cac-2e33aae2bfd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[2]/following::label[2]</value>
      <webElementGuid>7943b9dd-0758-44cf-8b08-c9a1aea421ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Healthcare Program'])[1]/preceding::label[1]</value>
      <webElementGuid>5b2920ef-5efd-45f8-982d-7b1e0ff47061</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apply for hospital readmission']/parent::*</value>
      <webElementGuid>751d7487-fba3-4c1a-a5b8-218b70cd88ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/label</value>
      <webElementGuid>c474a54a-9563-4d59-82a9-6fb8de371381</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = '
                             Apply for hospital readmission
                        ' or . = '
                             Apply for hospital readmission
                        ')]</value>
      <webElementGuid>8913bea7-2b6d-49fc-b79d-84ee321268b6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
