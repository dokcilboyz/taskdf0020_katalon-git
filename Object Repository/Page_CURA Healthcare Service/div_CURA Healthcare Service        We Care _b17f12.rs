<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_CURA Healthcare Service        We Care _b17f12</name>
   <tag></tag>
   <elementGuidId>2af93d59-71f3-4cf4-8dd5-72724e0a876f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-vertical-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//header[@id='top']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>925c1cf0-726a-42b5-8b47-91a36807914d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-vertical-center</value>
      <webElementGuid>2a04c3d2-c23e-4b78-9c08-41a8f0f07bc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        CURA Healthcare Service
        We Care About Your Health
        
        Make Appointment
    </value>
      <webElementGuid>cf5f3084-f515-4a4e-97ce-5521a4a5a30e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;top&quot;)/div[@class=&quot;text-vertical-center&quot;]</value>
      <webElementGuid>6a24e615-3493-44e8-8c4a-8cb6db28a9d9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//header[@id='top']/div</value>
      <webElementGuid>135d417a-11a8-4b0e-a3af-804cbb66280a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[1]/following::div[1]</value>
      <webElementGuid>4cc9cac0-4540-4381-979e-635821be1154</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::div[1]</value>
      <webElementGuid>1d5b2c56-4121-4eea-a70e-0cfb199e63b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
      <webElementGuid>44c449e9-6ff8-4d3a-a07b-a7eec8a23ad5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        CURA Healthcare Service
        We Care About Your Health
        
        Make Appointment
    ' or . = '
        CURA Healthcare Service
        We Care About Your Health
        
        Make Appointment
    ')]</value>
      <webElementGuid>15551f48-3aa0-48dc-b0d8-2ffa8ee7c249</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
